# AwesomeLogger
像系统自带Log一样简单的调用，日志形式更加清晰易懂，json化的数据可以直接打印出来，省去转换的麻烦，试试吧。
####引入：

compile 'cc.tracyzhang:AwesomeLogger:1.0.0'

####用法：

* ALogger.d(tag,obj)
* ALogger.i(tag,obj) 
* ALogger.w(tag,obj) 
* ALogger.e(tag,obj)


####示例：
![usage](http://git.oschina.net/uploads/images/2015/1225/192832_f5b2f0d9_83376.png "用法")
![sample](http://git.oschina.net/uploads/images/2015/1225/192918_31244229_83376.png "实例")